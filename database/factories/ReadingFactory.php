<?php

namespace Database\Factories;

use App\Models\Realty;
use App\Models\Reading;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Reading>
 */
class ReadingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'light'     => fake()->numberBetween(100, 300),
            'gas'       => fake()->numberBetween(1, 10),
            'heating'   => fake()->numberBetween(1, 5),
            'cold'      => fake()->numberBetween(3, 15),
            'hot'       => fake()->numberBetween(1, 10),
            'realty_id' => function () {
                return Realty::query()->inRandomOrder()->first()->id;
            },
        ];
    }
}

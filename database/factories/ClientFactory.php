<?php

namespace Database\Factories;

use App\Models\Client;
use App\Models\Realty;
use App\Dictionary\ClientRole;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Client>
 */
class ClientFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'first_name'  => fake()->firstName(),
            'last_name'   => fake()->lastName(),
            'middle_name' => fake()->title(),
            'phone'       => fake()->unique()->phoneNumber(),
            'role'        => ClientRole::ROLE_OWNER,
            'status'      => 1,
            'realty_id'   => function () {
                return Realty::query()->inRandomOrder()->first()->id;
            },
        ];
    }
}

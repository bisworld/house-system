<?php

namespace Database\Factories;

use App\Models\Realty;
use App\Dictionary\RealtyType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Realty>
 */
class RealtyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'account'  => fake()->unique()->numberBetween(1000, 9999),
            'street'   => fake()->streetName(),
            'house'    => fake()->numberBetween(10, 100),
            'flat'     => fake()->numberBetween(1, 300),
            'entrance' => fake()->numberBetween(1, 10),
            'floor'    => fake()->numberBetween(1, 30),
            'square'   => fake()->numberBetween(60, 150),
            'type'     => RealtyType::TYPE_LIVING
        ];
    }
}

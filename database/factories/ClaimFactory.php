<?php

namespace Database\Factories;

use App\Models\Claim;
use App\Models\Client;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Claim>
 */
class ClaimFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'subject'   => fake()->text(100),
            'body'      => fake()->text(500),
            'status'    => 1,
            'client_id' => function () {
                return Client::query()->inRandomOrder()->first()->id;
            },
        ];
    }
}

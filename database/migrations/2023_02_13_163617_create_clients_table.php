<?php

use App\Dictionary\ClientRole;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->enum('role', array_keys(ClientRole::ROLES));
            $table->string('first_name');
            $table->string('last_name');
            $table->string('middle_name');
            $table->string('phone')->nullable()->unique();
            $table->string('telegram')->nullable()->unique();
            $table->foreignId('realty_id')->constrained('realty')->onDelete('cascade');
            $table->smallInteger('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};

<?php

use App\Dictionary\RealtyType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('realty', function (Blueprint $table) {
            $table->id();
            $table->string('account')->unique();
            $table->enum('type', array_keys(RealtyType::TYPES));
            $table->string('street');
            $table->string('house');
            $table->string('flat');
            $table->string('entrance')->nullable();
            $table->string('floor')->nullable();
            $table->string('square')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('realty');
    }
};

<?php

namespace Database\Seeders;

use App\Models\Realty;
use Illuminate\Database\Seeder;

class RealtySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Realty::factory()->count(50)->create();
    }
}

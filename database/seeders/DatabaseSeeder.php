<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        User::factory()->create([
            'phone' => '79001234567',
        ]);

        $this->call([
            RealtySeeder::class,
            ClientSeeder::class,
            ClaimSeeder::class,
            ReadingSeeder::class,
        ]);
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\DoorController;
use App\Http\Controllers\Api\ClaimController;
use App\Http\Controllers\Api\ClientController;
use App\Http\Controllers\Api\TelegramController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('client', ClientController::class);

Route::apiResource('claim', ClaimController::class);

Route::apiResource('telegram', TelegramController::class);

Route::apiResource('door', DoorController::class);

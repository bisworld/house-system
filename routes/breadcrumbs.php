<?php

use App\Models\Claim;
use App\Models\Client;
use App\Models\Reading;
use App\Models\Realty;
use Diglactic\Breadcrumbs\Breadcrumbs;
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

/**
 * Admin
 */
Breadcrumbs::for('dashboard', function(BreadcrumbTrail $breadcrumbs) {
    $breadcrumbs->push('Админ Панель', route('dashboard'));
});



/**
 * Admin / Clients
 */
Breadcrumbs::for('client.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Клиенты', route('client.index'));
});

/**
 * Admin / Clients / Create Client
 */
Breadcrumbs::for('client.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('client.index');
    $breadcrumbs->push('Добавить Клиента', route('client.create'));
});

/**
 * Admin / Clients / [Client Name]
 */
Breadcrumbs::for('client.show', function(BreadcrumbTrail $breadcrumbs, Client $client)
{
    $breadcrumbs->parent('client.index');
    $breadcrumbs->push($client->first_name, route('client.show', $client->id));
});

/**
 * Admin / Clients / [Client Name] / Edit Client
 */
Breadcrumbs::for('client.edit', function(BreadcrumbTrail $breadcrumbs, Client $client)
{
    $breadcrumbs->parent('client.show', $client);
    $breadcrumbs->push('Редактировать Клиента', route('client.edit', $client->id));
});


/**
 * Admin / Claims
 */
Breadcrumbs::for('claim.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Обращения', route('claim.index'));
});

/**
 * Admin / Claims / Create Claim
 */
Breadcrumbs::for('claim.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('claim.index');
    $breadcrumbs->push('Добавить Обращение', route('claim.create'));
});

/**
 * Admin / Claims / [Claim Name]
 */
Breadcrumbs::for('claim.show', function(BreadcrumbTrail $breadcrumbs, Claim $claim)
{
    $breadcrumbs->parent('claim.index');
    $breadcrumbs->push($claim->subject, route('claim.show', $claim->id));
});

/**
 * Admin / Claims / [Claim Name] / Edit Claim
 */
Breadcrumbs::for('claim.edit', function(BreadcrumbTrail $breadcrumbs, Claim $claim)
{
    $breadcrumbs->parent('claim.show', $claim);
    $breadcrumbs->push('Редактировать Обращение', route('claim.edit', $claim->id));
});


/**
 * Admin / Realtys
 */
Breadcrumbs::for('realty.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Объекты', route('realty.index'));
});

/**
 * Admin / Realtys / Create Realty
 */
Breadcrumbs::for('realty.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('realty.index');
    $breadcrumbs->push('Добавить Объект', route('realty.create'));
});

/**
 * Admin / Realtys / [Realty Name]
 */
Breadcrumbs::for('realty.show', function(BreadcrumbTrail $breadcrumbs, Realty $realty)
{
    $breadcrumbs->parent('realty.index');
    $breadcrumbs->push($realty->flat, route('realty.show', $realty->id));
});

/**
 * Admin / Realtys / [Realty Name] / Edit Realty
 */
Breadcrumbs::for('realty.edit', function(BreadcrumbTrail $breadcrumbs, Realty $realty)
{
    $breadcrumbs->parent('realty.show', $realty);
    $breadcrumbs->push('Редактировать Объект', route('realty.edit', $realty->id));
});


/**
 * Admin / Readings
 */
Breadcrumbs::for('reading.index', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Показания', route('reading.index'));
});

/**
 * Admin / Readings / Create Reading
 */
Breadcrumbs::for('reading.create', function(BreadcrumbTrail $breadcrumbs)
{
    $breadcrumbs->parent('reading.index');
    $breadcrumbs->push('Добавить Показания', route('reading.create'));
});

/**
 * Admin / Readings / [Reading Name]
 */
Breadcrumbs::for('reading.show', function(BreadcrumbTrail $breadcrumbs, Reading $reading)
{
    $breadcrumbs->parent('reading.index');
    $breadcrumbs->push($reading->realty->full_name, route('reading.show', $reading->id));
});

/**
 * Admin / Readings / [Reading Name] / Edit Reading
 */
Breadcrumbs::for('reading.edit', function(BreadcrumbTrail $breadcrumbs, Reading $reading)
{
    $breadcrumbs->parent('reading.show', $reading);
    $breadcrumbs->push('Редактировать Показания', route('reading.edit', $reading->id));
});

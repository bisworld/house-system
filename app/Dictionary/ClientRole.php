<?php

namespace App\Dictionary;

interface ClientRole
{
    public const ROLE_OWNER  = 'owner';
    public const ROLE_TENANT = 'tenant';

    public const ROLES = [
        self::ROLE_OWNER  => 'Собственник',
        self::ROLE_TENANT => 'Арендатор',
    ];
}

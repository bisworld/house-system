<?php

namespace App\Dictionary;

interface TelegramText
{
    public const ACCOUNT = 'Авторизоваться';
    public const LIGHT = 'Свет';
    public const GAS = 'Газ';
    public const HOT = 'Горячая';
    public const COLD = 'Холодная';
    public const HEATING = 'Отопление';
    public const OPEN = 'Открыть шлагбаум';
    public const DOOR = 'door';

    public const TEXTS = [
        self::ACCOUNT,
        self::LIGHT,
        self::GAS,
        self::HOT,
        self::COLD,
        self::HEATING,
        self::OPEN,
    ];

    public const READINGS = [
        'light'   => self::LIGHT,
        'gas'     => self::GAS,
        'hot'     => self::HOT,
        'cold'    => self::COLD,
        'heating' => self::HEATING,
    ];
}

<?php

namespace App\Dictionary;

interface RealtyType
{
    public const TYPE_LIVING   = 'living';
    public const TYPE_COMMERCE = 'commerce';

    public const TYPES = [
        self::TYPE_LIVING   => 'Жилое',
        self::TYPE_COMMERCE => 'Коммерция',
    ];
}

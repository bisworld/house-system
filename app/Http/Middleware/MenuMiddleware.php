<?php

namespace App\Http\Middleware;

use Menu;
use Closure;
use Lavary\Menu\Builder;
use Illuminate\Http\Request;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        Menu::make('adminMenu', function ($menu) {
            /** @var $menu Builder */
            $menu->add('Админ Панель', ['route' => 'dashboard'])
                 ->prepend('<i class="fa fa-dashboard"></i> ');

            $menu->add('Объекты', ['route' => 'realty.index'])
                 ->prepend('<i class="fa fa-home"></i> ')
                 ->active('realty/*');

            $menu->add('Клиенты', ['route' => 'client.index'])
                 ->prepend('<i class="fa fa-users"></i> ')
                 ->active('client/*');

            $menu->add('Обращения', ['route' => 'claim.index'])
                ->prepend('<i class="fa fa-envelope"></i> ')
                ->active('claim/*');

            $menu->add('Показания', ['route' => 'reading.index'])
                ->prepend('<i class="fa fa-list"></i> ')
                ->active('reading/*');

            $menu->add('Документооборот', [])
                ->prepend('<i class="fa fa-file"></i> ');

            $menu->add('Настройки', [])
                ->prepend('<i class="fa fa-cogs"></i> ');
        });

        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Models\Claim;
use App\Models\Client;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreClaimRequest;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $client = Client::where(['telegram' => request()->get('telegram')])->firstOrFail();

        return response()->json($client->claims->all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClaimRequest $request
     * @return Response
     */
    public function store(StoreClaimRequest $request)
    {
        $client = Client::where(['telegram' => $request->get('telegram')])->firstOrFail();

        $client->claims()->create($request->validated());

        return response()->noContent();
    }

    /**
     * Display the specified resource.
     *
     * @param Claim $claim
     * @return JsonResponse
     */
    public function show(Claim $claim)
    {
        return response()->json($claim->toArray());
    }
}

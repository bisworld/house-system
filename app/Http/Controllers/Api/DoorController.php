<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Response;
use App\Dictionary\TelegramText;
use App\Http\Controllers\Controller;

class DoorController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        return response(cache()->pull(TelegramText::DOOR, 'false'));
    }
}

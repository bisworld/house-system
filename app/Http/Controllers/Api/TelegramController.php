<?php

namespace App\Http\Controllers\Api;

use App\Services\Telegram;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class TelegramController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Telegram $service
     * @return Response
     */
    public function store(Telegram $service)
    {
        $service->process();

        return response()->noContent();
    }
}

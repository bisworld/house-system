<?php

namespace App\Http\Controllers\Api;

use App\Models\Client;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\StoreClientRequest;

class ClientController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClientRequest $request
     * @return Response
     */
    public function store(StoreClientRequest $request)
    {
        Client::query()->create($request->validated());

        return response()->noContent();
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return JsonResponse
     */
    public function show(Client $client)
    {
        return response()->json($client->toArray());
    }
}

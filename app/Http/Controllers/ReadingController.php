<?php

namespace App\Http\Controllers;

use App\Models\Reading;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreReadingRequest;
use App\Http\Requests\UpdateReadingRequest;

class ReadingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('reading.index', [
            'readings' => Reading::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('reading.create', ['reading' => new Reading()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreReadingRequest $request
     * @return RedirectResponse
     */
    public function store(StoreReadingRequest $request)
    {
        Reading::query()->create($request->validated());

        return redirect()->route('reading.index')->with('success', 'Показания Добавлены!');
    }

    /**
     * Display the specified resource.
     *
     * @param Reading $reading
     * @return View
     */
    public function show(Reading $reading)
    {
        return view('reading.show', ['reading' => $reading]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Reading $reading
     * @return View
     */
    public function edit(Reading $reading)
    {
        return view('reading.edit', ['reading' => $reading]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateReadingRequest $request
     * @param Reading              $reading
     * @return RedirectResponse
     */
    public function update(UpdateReadingRequest $request, Reading $reading)
    {
        $reading->update($request->validated());

        return redirect()->route('reading.index')->with('success', 'Показания Обновлены!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Reading $reading
     * @return JsonResponse
     */
    public function destroy(Reading $reading)
    {
        $reading->delete();

        return response()->json(['status' => 'success', 'message' => 'Показания Удалены!']);
    }
}

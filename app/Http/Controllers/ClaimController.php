<?php

namespace App\Http\Controllers;

use App\Models\Claim;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreClaimRequest;
use App\Http\Requests\UpdateClaimRequest;

class ClaimController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('claim.index', [
            'claims' => Claim::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('claim.create', ['claim' => new Claim()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreClaimRequest $request
     * @return RedirectResponse
     */
    public function store(StoreClaimRequest $request)
    {
        Claim::query()->create($request->validated());

        return redirect()->route('claim.index')->with('success', 'Обращение Добавлено!');
    }

    /**
     * Display the specified resource.
     *
     * @param Claim $claim
     * @return View
     */
    public function show(Claim $claim)
    {
        return view('claim.show', ['claim' => $claim]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Claim $claim
     * @return View
     */
    public function edit(Claim $claim)
    {
        return view('claim.edit', ['claim' => $claim]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClaimRequest $request
     * @param Claim              $claim
     * @return RedirectResponse
     */
    public function update(UpdateClaimRequest $request, Claim $claim)
    {
        $claim->update($request->validated());

        return redirect()->route('claim.index')->with('success', 'Обращение Обновлено!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Claim $claim
     * @return JsonResponse
     */
    public function destroy(Claim $claim)
    {
        $claim->delete();

        return response()->json(['status' => 'success', 'message' => 'Обращение Удалено!']);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Realty;
use Illuminate\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use App\Http\Requests\StoreRealtyRequest;
use App\Http\Requests\UpdateRealtyRequest;

class RealtyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index()
    {
        return view('realty.index', [
            'realtys' => Realty::query()->paginate()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create()
    {
        return view('realty.create', ['realty' => new Realty()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRealtyRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRealtyRequest $request)
    {
        Realty::query()->create($request->validated());

        return redirect()->route('realty.index')->with('success', 'Объект Добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param Realty $realty
     * @return View
     */
    public function show(Realty $realty)
    {
        return view('realty.show', ['realty' => $realty]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Realty $realty
     * @return View
     */
    public function edit(Realty $realty)
    {
        return view('realty.edit', ['realty' => $realty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRealtyRequest $request
     * @param Realty              $realty
     * @return RedirectResponse
     */
    public function update(UpdateRealtyRequest $request, Realty $realty)
    {
        $realty->update($request->validated());

        return redirect()->route('realty.index')->with('success', 'Объект Обновлен!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Realty $realty
     * @return JsonResponse
     */
    public function destroy(Realty $realty)
    {
        $realty->delete();

        return response()->json(['status' => 'success', 'message' => 'Объект Удален!']);
    }
}

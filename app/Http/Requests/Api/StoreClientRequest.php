<?php

namespace App\Http\Requests\Api;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'role'        => ['required', 'string', 'in:owner,boss,staff,manager'],
            'first_name'  => ['required', 'string'],
            'last_name'   => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'street'      => ['required', 'string'],
            'house'       => ['required', 'string'],
            'flat'        => ['required', 'string'],
            'phone'       => ['required', 'string', 'unique:clients'],
            'telegram'    => ['required', 'string', 'unique:clients'],
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreReadingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'light'     => ['nullable', 'string'],
            'gas'       => ['nullable', 'string'],
            'heating'   => ['nullable', 'string'],
            'cold'      => ['nullable', 'string'],
            'hot'       => ['nullable', 'string'],
            'realty_id' => ['required', 'integer', 'exists:realty,id'],
        ];
    }
}

<?php

namespace App\Http\Requests;

use App\Dictionary\RealtyType;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRealtyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'account'  => ['required', 'string'],
            'street'   => ['required', 'string'],
            'house'    => ['required', 'string'],
            'flat'     => ['required', 'string'],
            'entrance' => ['nullable', 'string'],
            'floor'    => ['nullable', 'string'],
            'square'   => ['nullable', 'string'],
            'type'     => ['required', 'string', Rule::in(array_keys(RealtyType::TYPES))],
        ];
    }
}

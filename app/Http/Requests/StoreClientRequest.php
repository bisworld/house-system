<?php

namespace App\Http\Requests;

use App\Dictionary\ClientRole;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class StoreClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'role'        => ['required', 'string', Rule::in(array_keys(ClientRole::ROLES))],
            'first_name'  => ['required', 'string'],
            'last_name'   => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'phone'       => ['nullable', 'string', 'unique:clients'],
            'telegram'    => ['nullable', 'string', 'unique:clients'],
            'status'      => ['required', 'string'],
            'realty_id'   => ['required', 'integer', 'exists:realty,id'],
        ];
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Reading extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'realty_id', 'light', 'gas', 'heating', 'cold', 'hot'
    ];

    /**
     * @return BelongsTo
     */
    public function realty(): BelongsTo
    {
        return $this->belongsTo(Realty::class);
    }
}

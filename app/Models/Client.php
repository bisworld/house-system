<?php

namespace App\Models;

use App\Traits\Statusable;
use App\Dictionary\ClientRole;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory, Statusable;

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'role', 'first_name', 'last_name', 'middle_name', 'realty_id', 'phone', 'telegram', 'status'
    ];

    /**
     * @return HasMany
     */
    public function claims(): HasMany
    {
        return $this->hasMany(Claim::class);
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return string
     */
    public function getRoleNameAttribute(): string
    {
        return ClientRole::ROLES[$this->role];
    }

    /**
     * @return BelongsTo
     */
    public function realty(): BelongsTo
    {
        return $this->belongsTo(Realty::class);
    }
}

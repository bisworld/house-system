<?php

namespace App\Models;

use App\Dictionary\RealtyType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Realty extends Model
{
    use HasFactory;

    /**
     * @inheritdoc
     */
    protected $table = 'realty';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'account', 'type', 'street', 'house', 'flat', 'entrance', 'floor', 'square'
    ];

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return $this->street . ', д. ' . $this->house . ', кв. ' . $this->flat;
    }

    /**
     * @return string
     */
    public function getTypeNameAttribute(): string
    {
        return RealtyType::TYPES[$this->type];
    }

    /**
     * @return HasOne
     */
    public function client(): HasOne
    {
        return $this->hasOne(Client::class);
    }

    /**
     * @return HasMany
     */
    public function readings(): HasMany
    {
        return $this->hasMany(Reading::class);
    }
}

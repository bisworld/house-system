<?php

namespace App\Services;

use App\Models\Client;
use App\Models\Realty;
use Illuminate\Http\Request;
use App\Dictionary\TelegramText;
use Illuminate\Support\Collection;

class Telegram
{
    private const KEY = 'action';
    private const TEXT = 'text';
    private const CHAT = 'chat';

    /**
     * @var Collection
     */
    private Collection $message;

    public function __construct(Request $request)
    {
        if ($request->has('callback_query')) {
            $callback = collect($request->json('callback_query'));

            $this->message = collect($callback->get('message'));
        } else {
            $this->message = collect($request->json('message'));
        }
    }

    /**
     * @return void
     * @throws mixed
     */
    public function process(): void
    {
        if ($this->message->has('reply_markup')) {
            cache()->set(self::KEY, TelegramText::ACCOUNT);

            return;
        }

        if (in_array($this->message->get(self::TEXT), TelegramText::TEXTS)) {
            $this->processAction();
        } else {
            $this->processValue();
        }
    }

    /**
     * @return void
     * @throws mixed
     */
    private function processAction(): void
    {
        if (TelegramText::OPEN == $this->message->get(self::TEXT)) {
            cache()->add(TelegramText::DOOR, 'true');
        } else {
            cache()->set(self::KEY, $this->message->get(self::TEXT));
        }
    }

    /**
     * @return void
     */
    private function processValue(): void
    {
        if (! cache()->has(self::KEY)) {
            return;
        }

        $action = cache()->pull(self::KEY);

        switch ($action) {
            case TelegramText::ACCOUNT:
                $this->processAccount();
                break;

            case TelegramText::LIGHT:
            case TelegramText::GAS:
            case TelegramText::HEATING:
            case TelegramText::COLD:
            case TelegramText::HOT:
                $this->processReading($action);
                break;

            case TelegramText::OPEN:
                // Ожидаем запрос от двери
                break;
        }
    }

    /**
     * @return void
     */
    private function processAccount(): void
    {
        $realty = Realty::where(['account' => $this->message->get(self::TEXT)])->firstOrFail();
        $realty->client->update(['telegram' => $this->getTelegram()]);
    }

    private function processReading(string $action): void
    {
        $column = array_search($action, TelegramText::READINGS);

        $this->getClient()->realty
            ->readings()->whereMonth('created_at', date('m'))
            ->updateOrCreate([], [$column => $this->message->get(self::TEXT)]);
    }

    /**
     * @return Client
     */
    private function getClient(): Client
    {
        return Client::where(['telegram' => $this->getTelegram()])->first();
    }

    /**
     * @return integer
     */
    private function getTelegram(): int
    {
        return collect($this->message->get(self::CHAT))->get('id');
    }
}


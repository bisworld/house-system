@if (session()->has('errors'))
    <div class="message error">
{{--        <span>Ошибка!</span> Некоторые поля заполнены неверно!--}}
        <ul class="list">
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@elseif (session()->has('error'))
    <div class="message error">
        <span>Ошибка! {{ session('error') }}</span>
    </div>
@elseif (session()->has('success'))
    <div class="message success">
        <span>Успех! {{ session('success') }}</span>
    </div>
@endif

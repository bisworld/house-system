@section('title', 'Редактирование Обращения')
@section('section', 'Управление Обращениями')
@section('breadcrumbs', Breadcrumbs::render('claim.edit', $claim))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Обращения</h2>
        </header>

        {!! Form::model($claim, ['route' => ['claim.update', $claim->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('claim.form', ['claim' => $claim])
        {!! Form::close() !!}
    </section>
</x-app-layout>

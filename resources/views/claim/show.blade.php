@section('title', 'Просмотр Обращения')
@section('section', 'Управление Обращениями')
@section('breadcrumbs', Breadcrumbs::render('claim.show', $claim))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Обращения</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Тема:</span>
                    <span class="inp">{{ $claim->subject }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Клиент:</span>
                    <span class="inp">{{ $claim->client->full_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Содержимое:</span>
                    <span class="inp">{{ $claim->body }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Статус:</span>
                    <span class="inp">{{ $claim->status_text }}</span>
                </div>
                <div class="col form-item"></div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>

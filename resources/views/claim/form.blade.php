<div class="row">
    <div class="col form-item">
        {!! Form::label('subject', 'Тема:') !!}
        {!! Form::text('subject', null, ['class' => 'inp', 'placeholder' => 'Тема обращения', 'required']) !!}
    </div>

    <div class="col form-item">
        {!! Form::label('client_id', 'Клиент:') !!}
        {!! Form::select('client_id', \App\Models\Client::all()->pluck('full_name', 'id')->toArray(), null, ['class' => 'select', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('body', 'Содержимое:') !!}
        {!! Form::textarea('body', null, ['class' => 'inp', 'placeholder' => 'Содержимое обращения', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('status', 'Статус:') !!}
        {!! Form::select('status', $claim->statusList(), null, ['class' => 'select', 'required']) !!}
    </div>
    <div class="col form-item"></div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>

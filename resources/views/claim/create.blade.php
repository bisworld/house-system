@section('title', 'Добавление Обращения')
@section('section', 'Управление Обращениями')
@section('breadcrumbs', Breadcrumbs::render('claim.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Обращения</h2>
        </header>

        {!! Form::open(['route' => 'claim.store', 'class' => 'form body-tile']) !!}
        @include('claim.form', ['claim' => $claim])
        {!! Form::close() !!}
    </section>
</x-app-layout>

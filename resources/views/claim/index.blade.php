@section('title', 'Обзор Обращений')
@section('section', 'Управление Обращениями')
@section('breadcrumbs', Breadcrumbs::render('claim.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Обращений</h2>

            <nav class="controls">
                <a href="{!! route('claim.create') !!}"><i class="fa fa-plus"></i> Добавить Обращение</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Тема</th>
                <th>Клиент</th>
                <th class="center">Статус</th>
                <th>Дата Создания</th>
                <th>Дата Изменения</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($claims as $claim)
                <tr>
                    <td>{{ $claim->id }}.</td>
                    <td>{{ $claim->subject }}</td>
                    <td>{{ $claim->client->full_name }}</td>
                    <td class="center">{!! $claim->status_icon !!}</td>
                    <td>@datetime($claim->created_at)</td>
                    <td>@datetime($claim->updated_at)</td>
                    <td class="action-column">
                        <a href="{!! route('claim.show', [$claim->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('claim.edit', [$claim->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $claim->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="7">
                        <div class="message warning" role="alert">Обращения не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $claims->links() }}
    </section>
</x-app-layout>

@section('title', 'Обзор Объектов')
@section('section', 'Управление Объектами')
@section('breadcrumbs', Breadcrumbs::render('realty.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Объектов</h2>

            <nav class="controls">
                <a href="{!! route('realty.create') !!}"><i class="fa fa-plus"></i> Добавить Объект</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Лицевой Счет</th>
                <th>Тип</th>
                <th>Улица</th>
                <th>Дом</th>
                <th>Квартира</th>
                <th>Дата Создания</th>
                <th>Дата Изменения</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($realtys as $realty)
                <tr>
                    <td>{{ $realty->id }}.</td>
                    <td>{{ $realty->account }}</td>
                    <td>{{ $realty->type_name }}</td>
                    <td>{{ $realty->street }}</td>
                    <td>{{ $realty->house }}</td>
                    <td>{{ $realty->flat }}</td>
                    <td>@datetime($realty->created_at)</td>
                    <td>@datetime($realty->updated_at)</td>
                    <td class="action-column">
                        <a href="{!! route('realty.show', [$realty->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('realty.edit', [$realty->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $realty->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Объекты не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $realtys->links() }}
    </section>
</x-app-layout>

@section('title', 'Добавление Объекта')
@section('section', 'Управление Объектами')
@section('breadcrumbs', Breadcrumbs::render('realty.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Объекта</h2>
        </header>

        {!! Form::open(['route' => 'realty.store', 'class' => 'form body-tile']) !!}
        @include('realty.form', ['realty' => $realty])
        {!! Form::close() !!}
    </section>
</x-app-layout>

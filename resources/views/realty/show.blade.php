@section('title', 'Просмотр Объекта')
@section('section', 'Управление Объектами')
@section('breadcrumbs', Breadcrumbs::render('realty.show', $realty))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Объекта</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Лицевой Счет:</span>
                    <span class="inp">{{ $realty->account }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Тип Недвижимости:</span>
                    <span class="inp">{{ $realty->type_name }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Улица:</span>
                    <span class="inp">{{ $realty->street }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дом:</span>
                    <span class="inp">{{ $realty->house }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Квартира:</span>
                    <span class="inp">{{ $realty->flat }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Подъезд:</span>
                    <span class="inp">{{ $realty->entrance }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Этаж:</span>
                    <span class="inp">{{ $realty->floor }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Площадь:</span>
                    <span class="inp">{{ $realty->square }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Создания:</span>
                    <span class="inp">{{ $realty->created_at }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Изменения:</span>
                    <span class="inp">{{ $realty->updated_at }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>

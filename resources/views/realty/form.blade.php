<div class="row">
    <div class="col form-item">
        {!! Form::label('account', 'Лицевой Счет:') !!}
        {!! Form::text('account', null, ['class' => 'inp', 'placeholder' => '1234', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('type', 'Тип Недвижимости:') !!}
        {!! Form::select('type', \App\Dictionary\RealtyType::TYPES, null, ['class' => 'select', 'required']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('street', 'Улица:') !!}
        {!! Form::text('street', null, ['class' => 'inp', 'placeholder' => 'Зиповская', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('house', 'Дом:') !!}
        {!! Form::text('house', null, ['class' => 'inp', 'placeholder' => '36', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('flat', 'Квартира:') !!}
        {!! Form::text('flat', null, ['class' => 'inp', 'placeholder' => '56']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('entrance', 'Подъезд:') !!}
        {!! Form::text('entrance', null, ['class' => 'inp', 'placeholder' => '1']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('floor', 'Этаж:') !!}
        {!! Form::text('floor', null, ['class' => 'inp', 'placeholder' => '12']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('square', 'Площадь:') !!}
        {!! Form::text('square', null, ['class' => 'inp', 'placeholder' => '56.6']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>

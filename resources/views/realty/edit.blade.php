@section('title', 'Редактирование Объекта')
@section('section', 'Управление Объектами')
@section('breadcrumbs', Breadcrumbs::render('realty.edit', $realty))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Объекта</h2>
        </header>

        {!! Form::model($realty, ['route' => ['realty.update', $realty->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('realty.form', ['realty' => $realty])
        {!! Form::close() !!}
    </section>
</x-app-layout>

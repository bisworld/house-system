<div class="row">
    <div class="col form-item">
        {!! Form::label('realty_id', 'Объект:') !!}
        {!! Form::select('realty_id', \App\Models\Realty::all()->pluck('full_name', 'id')->toArray(), null, ['class' => 'select', 'required']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('light', 'Свет:') !!}
        {!! Form::text('light', null, ['class' => 'inp', 'placeholder' => '214']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('gas', 'Газ:') !!}
        {!! Form::text('gas', null, ['class' => 'inp', 'placeholder' => '12']) !!}
    </div>
</div>

<div class="row">
    <div class="col form-item">
        {!! Form::label('heating', 'Отопление:') !!}
        {!! Form::text('heating', null, ['class' => 'inp', 'placeholder' => '1.2']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('cold', 'Холодная Вода:') !!}
        {!! Form::text('cold', null, ['class' => 'inp', 'placeholder' => '6']) !!}
    </div>
    <div class="col form-item">
        {!! Form::label('hot', 'Горячая Вода:') !!}
        {!! Form::text('hot', null, ['class' => 'inp', 'placeholder' => '2']) !!}
    </div>
</div>

<footer class="row footer-tile">
    <a href="{{ URL::previous() }}" class="button light-red">Отмена</a>
    <input type="submit" value="Сохранить" class="button success">
</footer>

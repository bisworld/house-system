@section('title', 'Добавление Показаний')
@section('section', 'Управление Показаниями')
@section('breadcrumbs', Breadcrumbs::render('reading.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Показаний</h2>
        </header>

        {!! Form::open(['route' => 'reading.store', 'class' => 'form body-tile']) !!}
        @include('reading.form', ['reading' => $reading])
        {!! Form::close() !!}
    </section>
</x-app-layout>

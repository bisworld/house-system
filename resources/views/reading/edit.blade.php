@section('title', 'Редактирование Показаний')
@section('section', 'Управление Показаниями')
@section('breadcrumbs', Breadcrumbs::render('reading.edit', $reading))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Показаний</h2>
        </header>

        {!! Form::model($reading, ['route' => ['reading.update', $reading->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('reading.form', ['reading' => $reading])
        {!! Form::close() !!}
    </section>
</x-app-layout>

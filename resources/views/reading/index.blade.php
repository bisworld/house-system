@section('title', 'Обзор Показаний')
@section('section', 'Управление Показаниями')
@section('breadcrumbs', Breadcrumbs::render('reading.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Показаний</h2>

            <nav class="controls">
                <a href="{!! route('reading.create') !!}"><i class="fa fa-plus"></i> Добавить Показания</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Объект</th>
                <th>Газ</th>
                <th>Свет</th>
                <th>Отопление</th>
                <th>Холодная Вода</th>
                <th>Горячая Вода</th>
                <th>Дата Подачи</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($readings as $reading)
                <tr>
                    <td>{{ $reading->id }}.</td>
                    <td>{{ $reading->realty->full_name }}</td>
                    <td>{{ $reading->gas }}</td>
                    <td>{{ $reading->light }}</td>
                    <td>{{ $reading->heating }}</td>
                    <td>{{ $reading->cold }}</td>
                    <td>{{ $reading->hot }}</td>
                    <td>@datetime($reading->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('reading.show', [$reading->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('reading.edit', [$reading->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $reading->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="9">
                        <div class="message warning" role="alert">Показания не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $readings->links() }}
    </section>
</x-app-layout>

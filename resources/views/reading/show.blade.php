@section('title', 'Просмотр Показаний')
@section('section', 'Управление Показаниями')
@section('breadcrumbs', Breadcrumbs::render('reading.show', $reading))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Просмотр</strong> Показаний</h2>
        </header>

        <article class="body-tile form">
            <div class="row">
                <div class="col form-item">
                    <span class="label">Объект:</span>
                    <span class="inp">{{ $reading->realty->full_name }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Свет:</span>
                    <span class="inp">{{ $reading->light }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Газ:</span>
                    <span class="inp">{{ $reading->gas }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Отопление:</span>
                    <span class="inp">{{ $reading->heating }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Холодная Вода:</span>
                    <span class="inp">{{ $reading->cold }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Горячая Вода:</span>
                    <span class="inp">{{ $reading->hot }}</span>
                </div>
            </div>

            <div class="row">
                <div class="col form-item">
                    <span class="label">Дата Подачи:</span>
                    <span class="inp">{{ $reading->created_at }}</span>
                </div>
                <div class="col form-item">
                    <span class="label">Дата Изменения:</span>
                    <span class="inp">{{ $reading->updated_at }}</span>
                </div>
            </div>

            <footer class="row footer-tile">
                <a href="{{ URL::previous() }}" class="button">Назад</a>
            </footer>
        </article>
    </section>
</x-app-layout>

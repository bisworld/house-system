@section('title', 'Обзор Клиентов')
@section('section', 'Управление Клиентами')
@section('breadcrumbs', Breadcrumbs::render('client.index'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Обзор</strong> Клиентов</h2>

            <nav class="controls">
                <a href="{!! route('client.create') !!}"><i class="fa fa-plus"></i> Добавить Клиента</a>
            </nav>
        </header>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Отчество</th>
                <th>Телефон</th>
                <th>Объект</th>
                <th>Роль</th>
                <th class="center">Статус</th>
                <th>Дата Регистрации</th>
                <th class="action-column">Действия</th>
            </tr>
            </thead>
            <tbody>
            @forelse($clients as $client)
                <tr>
                    <td>{{ $client->id }}.</td>
                    <td>{{ $client->first_name }}</td>
                    <td>{{ $client->last_name }}</td>
                    <td>{{ $client->middle_name }}</td>
                    <td>{{ $client->phone }}</td>
                    <td>{{ $client->realty->full_name }}</td>
                    <td>{{ $client->role_name }}</td>
                    <td class="center">{!! $client->status_icon !!}</td>
                    <td>@datetime($client->created_at)</td>
                    <td class="action-column">
                        <a href="{!! route('client.show', [$client->id]) !!}" class="button with-icon"
                           data-tooltip="Просмотр"><i class="fa fa-eye"></i></a>
                        <a href="{!! route('client.edit', [$client->id]) !!}" class="button with-icon"
                           data-tooltip="Изменить"><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0)" class="button with-icon" data-tooltip="Удалить"
                           data-delete="{{ $client->id }}"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="10">
                        <div class="message warning" role="alert">Клиенты не найдены!</div>
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>

        {{ $clients->links() }}
    </section>
</x-app-layout>

@section('title', 'Редактирование Клиента')
@section('section', 'Управление Клиентами')
@section('breadcrumbs', Breadcrumbs::render('client.edit', $client))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Редактирование</strong> Клиента</h2>
        </header>

        {!! Form::model($client, ['route' => ['client.update', $client->id], 'method' => 'PUT', 'class' => 'form body-tile']) !!}
        @include('client.form', ['client' => $client])
        {!! Form::close() !!}
    </section>
</x-app-layout>

@section('title', 'Добавление Клиента')
@section('section', 'Управление Клиентами')
@section('breadcrumbs', Breadcrumbs::render('client.create'))

<x-app-layout>
    <section class="tile">
        <header class="header-tile">
            <h2><strong>Добавление</strong> Клиента</h2>
        </header>

        {!! Form::open(['route' => 'client.store', 'class' => 'form body-tile']) !!}
        @include('client.form', ['client' => $client])
        {!! Form::close() !!}
    </section>
</x-app-layout>
